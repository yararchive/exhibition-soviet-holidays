
$(document).ready(function() {
  $(function() {
    $('a[href*=#]:not([href=#])').click(function(e) {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        var scrollTime = parseInt(Math.abs(target.offset().top - $(document).scrollTop()) / 10 * 3);
        if (scrollTime < 2000) {
          scrollTime = 2000;
        }
        // console.log(scrollTime);
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, scrollTime);
          return false;
        }
      }
    });
  });

  $("#force-open .button").click(function() {
    $(".not-supported").fadeOut(500, function(e) {
      $(this).remove();
    });
  });

  loaderTexts = new Array("Загружаю выставку", "Загрузка может занять продолжительное время", "Пожалуйста, подождите");
  textsCount = textNumber =loaderTexts.length;

  changeLoaderText = function() {
    if ($("#loader-screen").css("display") === "block") {
      textNumber++;
      newText = loaderTexts[textNumber % textsCount]
      $("#loader-text").fadeOut(500, function() {
        $("#loader-text").text(newText).fadeIn(500);
      });
      setTimeout(changeLoaderText, 3000);
    }
  };
  setTimeout(changeLoaderText, 3000);

  $(window).load(function() {
    $("#loader-screen").fadeOut(500);
    $("html, body").delay(500).animate({ scrollTop: 0 }, 1000).animate({ scrollTop: 10 }, 1000);
    $("#part0-banner .parallax img, #part1-banner .parallax img, #part2-banner .parallax img, #part3-banner .parallax img, #part4-banner .parallax img").attr("src", "images/bg_header.jpg");
    $("#part0-bottom-parallax .parallax img").attr("src", "images/bg_footer.jpg");
  });

  $(".part001").click(function(e) {
    $('.part').delay(500).fadeOut(1000);
    $('#part1').delay(500).fadeIn(1000);
    $("html, body").delay(500).animate({ scrollTop: 0 }, 1000).animate({ scrollTop: 10 }, 1000);
  });

  $(".part002").click(function(e) {
    $('.part').delay(500).fadeOut(1000);
    $('#part2').delay(500).fadeIn(1000);
    $("html, body").delay(500).animate({ scrollTop: 0 }, 1000).animate({ scrollTop: 10 }, 1000);
  });

  $(".part003").click(function(e) {
    $('.part').delay(500).fadeOut(1000);
    $('#part3').delay(500).fadeIn(1000);
    $("html, body").delay(500).animate({ scrollTop: 0 }, 1000).animate({ scrollTop: 10 }, 1000);
  });

  $(".part004").click(function(e) {
    $('.part').delay(500).fadeOut(1000);
    $('#part4').delay(500).fadeIn(1000);
    $("html, body").delay(500).animate({ scrollTop: 0 }, 1000).animate({ scrollTop: 10 }, 1000);
  });

  $(".part000").click(function(e) {
    $('.part').delay(500).fadeOut(1000);
    $('#part0').delay(500).fadeIn(1000);
    $("html, body").delay(500).animate({ scrollTop: 0 }, 1000).animate({ scrollTop: 10 }, 1000);
    $("#part0-banner .parallax img").attr("src", "images/bg_header.jpg");
    $("#part0-bottom-parallax .parallax img").attr("src", "images/bg_footer.jpg");
  });
});